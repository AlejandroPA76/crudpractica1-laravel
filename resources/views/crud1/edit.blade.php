@extends('plantillas.layout')

@section('cont')
<form action="/cliente/{{$cli->id}}" method="POST" >
@csrf
@method('PUT')	
<div class="form-group">
            <label>introduce tu nombre:</label>
            <input type="text" value="{{$cli->Name}}" name="nombre" required>
    </div>
<div class="form-group">
            <label>introduce tus apellidos:</label>
            <input type="text" value="{{$cli->Apellido}}" name="apellidos" required>
    </div>
   

    <div class="form-group">
            <label>introduce tu edad:</label>
            <input type="number" value="{{$cli->Edad}}" name="edad" required>
    </div>

 <a href="/cliente" class="btn btn-info">salir</a>
 
<button class="btn btn-success" type="submit">enviar</button>
</form>

@endsection



