@extends('plantillas.layout')

@section('cont')
   <a href="cliente/create" class="btn btn-primary">agregar</a>
   <br>
   <br>
<table class="table">

  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">nombre</th>
      <th scope="col">apellido</th>
      <th scope="col">edad</th>
      <th scope="col">accion</th>

    </tr>
  </thead>
  <tbody>

    @foreach($clis as $c)
    <tr>
      <td>{{$c->id}}</td>
      <td>{{$c->Name}}</td>
      <td>{{$c->Apellido}}</td>
      <td>{{$c->Edad}}</td>

      <td>
        <div class="btn-group btn">

        <div class="btn-group mr-2">
        <a class="btn btn-primary" href="cliente/{{$c->id}}">ver</a>
        </div>
        <div class="btn-group mr-2">
          <a class="btn btn-warning" href="cliente/{{$c->id}}/edit">editar</a>
        </div>
        
        <form action="cliente/{{$c->id}}" method="POST">
          @csrf
          @method('delete')
          <button type="submit" class="btn btn-danger">Eliminar</button>          
        </form>
              </div>
      </td>
      
    </tr>
    
   @endforeach
  </tbody>
</table>
@stop