<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clis = Cliente::orderBy('created_at','desc')->get();
        return view('crud1.index',compact('clis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crud1.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $cli = new Cliente;
    $cli->Name = $request->input('nombre');
    $cli->Apellido = $request->input('apellidos');
    $cli->Edad = $request->input('edad');
    $cli->save();
    return redirect('cliente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $cli2 = Cliente::find($id);
        // return $cli;
        return view('crud1.show',compact('cli2'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cli = Cliente::find($id);
        return view('crud1.edit',compact('cli'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uptCliente = Cliente::find($id);
        $uptCliente->Name = $request->input('nombre');
        $uptCliente->Apellido = $request->input('apellidos');
        $uptCliente->Edad = $request->input('edad');
        $uptCliente->save();
          return redirect('cliente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delCli = Cliente::find($id);
        //return $delCli;
        $delCli->delete();
        return redirect('cliente');
    }
}
